import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AnalyseModule } from './endpoint-modules/analyse/analyse.module';
import { ContextFreeGrammarTestModule } from './modules/context-free-grammar-test/context-free-grammar-test.module';
import { ReduceGrammarModule } from './modules/reduce-grammar/reduce-grammar.module';
import { TransformGrammarUrlModule } from './modules/transform-grammar-url/transform-grammar-url.module';
import { CreateSymbolGroupsModule } from './modules/create-symbol-groups/create-symbol-groups.module';
import { SetFirstModule } from './modules/set-first/set-first.module';
import { SetFollowModule } from './modules/set-follow/set-follow.module';
import { GrammarSetsModule } from './modules/grammar-sets/grammar-sets.module';
import { AutomateCreatorModule } from './modules/automate-creator/automate-creator.module';
import { AutomateTableCreatorModule } from './modules/automate-table-creator/automate-table-creator.module';
import { TextTestModule } from './endpoint-modules/text-test/text-test.module';
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from 'path'
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    AnalyseModule,
    ContextFreeGrammarTestModule,
    ReduceGrammarModule,
    TransformGrammarUrlModule,
    CreateSymbolGroupsModule,
    SetFirstModule,
    SetFollowModule,
    GrammarSetsModule,
    AutomateCreatorModule,
    AutomateTableCreatorModule,
    TextTestModule,
    ServeStaticModule.forRoot({
      serveRoot: '/',
      rootPath: join(__dirname, '..', 'client'),
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
