import { Body, Controller, Get, Inject, Post, Query } from "@nestjs/common";
import { AnalyseService } from './analyse.service';
import Grammar from "../../interfaces/grammar.interface";
import { TransformGrammarUrlService } from "../../modules/transform-grammar-url/transform-grammar-url.service";

@Controller('api/analyse')
export class AnalyseController {
  @Inject(TransformGrammarUrlService)
  private readonly urlService: TransformGrammarUrlService
  @Inject(AnalyseService)
  private readonly analyseService: AnalyseService

  @Get('/grammar/')
  public analyseQueryGrammar(@Query() grammar: Grammar): string {
    this.urlService.transformGrammar(grammar)
    const result = this.analyseService.analyseGrammar(grammar)
    return JSON.stringify(result, null, 3)
  }

  @Post('/grammar/')
  public analyseGrammar(@Body() grammar: Grammar): string {
    this.urlService.transformGrammar(grammar)
    const result = this.analyseService.analyseGrammar(grammar)
    return JSON.stringify(result, null, 3)
  }
}
