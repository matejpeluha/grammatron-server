import { Module } from '@nestjs/common';
import { AnalyseService } from './analyse.service';
import { AnalyseController } from './analyse.controller';
import { ContextFreeGrammarTestModule } from "../../modules/context-free-grammar-test/context-free-grammar-test.module";
import { ReduceGrammarModule } from "../../modules/reduce-grammar/reduce-grammar.module";
import { TransformGrammarUrlModule } from "../../modules/transform-grammar-url/transform-grammar-url.module";
import { SetFirstModule } from "../../modules/set-first/set-first.module";
import { SetFollowModule } from "../../modules/set-follow/set-follow.module";
import { AutomateCreatorModule } from "../../modules/automate-creator/automate-creator.module";
import { AutomateTableCreatorModule } from "../../modules/automate-table-creator/automate-table-creator.module";

@Module({
  imports: [TransformGrammarUrlModule, ContextFreeGrammarTestModule, ReduceGrammarModule, SetFirstModule, SetFollowModule, AutomateCreatorModule, AutomateTableCreatorModule],
  controllers: [AnalyseController],
  providers: [AnalyseService],
  exports: [],
})
export class AnalyseModule {}
