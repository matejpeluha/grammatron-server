import { Inject, Injectable } from "@nestjs/common";
import Grammar from "../../interfaces/grammar.interface";
import { ContextFreeGrammarTestService } from "../../modules/context-free-grammar-test/context-free-grammar-test.service";
import { ReduceGrammarService } from "../../modules/reduce-grammar/reduce-grammar.service";
import { SetFirstService } from "../../modules/set-first/set-first.service";
import { SetFollowService } from "../../modules/set-follow/set-follow.service";
import { SetsCollection } from "../../interfaces/collections.interface";
import { AutomateCreatorService } from "../../modules/automate-creator/automate-creator.service";
import AutomateResponse from "../../interfaces/automate-response.interface";



@Injectable()
export class AnalyseService {
  @Inject(ContextFreeGrammarTestService)
  private readonly contextFreeService: ContextFreeGrammarTestService
  @Inject(ReduceGrammarService)
  private readonly reduceGrammarService: ReduceGrammarService
  @Inject(SetFirstService)
  private readonly firstService: SetFirstService
  @Inject(SetFollowService)
  private readonly followService: SetFollowService
  @Inject(AutomateCreatorService)
  private readonly automateService: AutomateCreatorService

  public analyseGrammar (grammar: Grammar): AutomateResponse {
    const isGrammarContextFree = this.contextFreeService.isGrammarContextFree(grammar)
    if (!isGrammarContextFree) {
      return { isGrammarContextFree, grammar }
    }
    this.reduceGrammarService.reduceGrammar(grammar)
    const { first, follow } = this.createSets(grammar)

    const { table, numberOfStates, states} = this.automateService.createAutomate(grammar, first)

    return { table, numberOfStates, states, follow, first, grammar, isGrammarContextFree }
  }

  private createSets (grammar: Grammar): SetsCollection {
    const first = this.firstService.createSetFirst(grammar)
    const follow = this.followService.createSetFollow(grammar, first)
    return { first, follow }
  }
}
