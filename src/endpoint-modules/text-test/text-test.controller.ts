import { Body, Controller, Inject, Post } from "@nestjs/common";
import { TextTestService } from "./text-test.service";
import { TextTestBody } from "../../interfaces/text-test.interface";
import { TableStateItem } from "../../interfaces/automate-table.interface";
import { TransformGrammarUrlService } from "../../modules/transform-grammar-url/transform-grammar-url.service";

@Controller('/api/text-test')
export class TextTestController {
  @Inject(TextTestService)
  private readonly textTestService: TextTestService
  @Inject(TransformGrammarUrlService)
  private readonly urlService: TransformGrammarUrlService

  @Post('/')
  public postTextTest (@Body() body: TextTestBody) {
    this.urlService.transformGrammar(body.grammar)
    this.removeWhiteSpaces(body)
    const result = this.textTestService.getTextTestResult(body)
    return JSON.stringify(result, null, 3)
  }

  private removeWhiteSpaces (body: TextTestBody) {
    body.tableStates = body.tableStates.map((items: TableStateItem[]) => {
      return items.map((item: TableStateItem) => {
        if (item.operation === 'reduce' && item.reduceRule) {
          item.reduceRule.leftSide = item.reduceRule.leftSide.replace(/\s/g, '')
          item.reduceRule.rightSide = item.reduceRule.rightSide.map(symbol => symbol.replace(/\s/g, ''))
        }
        return item
      })
    })
  }
}
