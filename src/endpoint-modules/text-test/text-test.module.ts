import { Module } from '@nestjs/common';
import { TextTestController } from './text-test.controller';
import { TextTestService } from './text-test.service';
import { AutomateTableCreatorModule } from "../../modules/automate-table-creator/automate-table-creator.module";
import { TransformGrammarUrlModule } from "../../modules/transform-grammar-url/transform-grammar-url.module";

@Module({
  imports: [AutomateTableCreatorModule, TransformGrammarUrlModule],
  controllers: [TextTestController],
  providers: [TextTestService]
})
export class TextTestModule {}
