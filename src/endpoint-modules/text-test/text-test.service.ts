import { Inject, Injectable } from "@nestjs/common";
import { TextTestBody } from "../../interfaces/text-test.interface";
import { AutomateTableCreatorService } from "../../modules/automate-table-creator/automate-table-creator.service";
import { AutomateTableStates, TableStateItem } from "../../interfaces/automate-table.interface";
import GrammarRule from "../../interfaces/grammar-rule.interface";
import Constants from "../../constants";
import { GrammarTreeNode } from "../../interfaces/grammar-tree.interface";
import Grammar from "../../interfaces/grammar.interface";
import { TextTestResponse } from "../../interfaces/text-test-response.interface";
import { AnalysisStep } from "../../interfaces/analysis-steps.interface";

@Injectable()
export class TextTestService {
  @Inject(AutomateTableCreatorService)
  private readonly tableService: AutomateTableCreatorService
  private grammar: Grammar
  private tableStates: AutomateTableStates
  private allSymbols: string[][]
  private text: string[]
  private nextTextSymbols?: string[]
  private ruleStack: GrammarRule[]
  private analysisSteps: AnalysisStep[]

  public getTextTestResult ({ grammar, text, tableStates }: TextTestBody): TextTestResponse {
    if (!text || text.length === 0) {
      return { result: false, error: 'missing_text' }
    } else if (!tableStates || tableStates.length === 0) {
      return { result: false, error: 'missing_table' }
    }

    this.grammar = grammar
    this.ruleStack = []
    this.analysisSteps = []
    this.tableStates = tableStates
    this.text = text
    this.allSymbols = this.tableService.getAllTableSymbols(tableStates)
    const isCollisionPresent = this.tableService.isCollisionPresent(tableStates, this.allSymbols)

    if (isCollisionPresent) {
      return { result: false, error: 'is_collision_present' }
    }

    return this.runTest()
  }

  private runTest (): TextTestResponse {
    const stateStack: number[] = [ 0 ]
    this.loadNextTextSymbols()

    while (stateStack.length > 0) {
      const tableState: TableStateItem[] = this.tableStates[stateStack[0]]
      const tableItem = tableState.find(item => JSON.stringify(item.symbols) === JSON.stringify(this.nextTextSymbols))

      if (!tableItem) {
        return { result: false, error: 'text_not_in_grammar'}
      }
      if (tableItem.operation === 'shift' && tableItem.gotoState) {
        this.addAnalysisStep(tableItem, stateStack)
        stateStack.unshift(tableItem.gotoState)
        this.loadNextTextSymbols()
      } else if (tableItem.operation === "reduce" && tableItem.reduceRule && this.isRuleCorrect(tableItem.reduceRule)) {
        this.addAnalysisStep(tableItem, stateStack)
        const reduceResult = this.runReduceOperation(tableItem, stateStack)
        if (reduceResult && !reduceResult.result){
          return reduceResult
        }
      } else if (tableItem.operation === 'accept') {
        this.addAnalysisStep(tableItem, stateStack)
        const tree = this.constructGrammarTree()
        return { result: true, analysisSteps: this.analysisSteps, tree, ruleStack: this.ruleStack }
      } else {
        return { result: false, error: 'text_not_in_grammar'}
      }
    }

    return { result: false, error: 'text_not_in_grammar'}
  }

  private addAnalysisStep (tableItem: TableStateItem, stateStack: number[]): void {
    this.analysisSteps.push({
      stateStack: [...stateStack].reverse(),
      nextSymbols:  this.nextTextSymbols ? [this.nextTextSymbols[0], ...this.text] : [...this.text],
      action: tableItem
    })
  }

  private constructGrammarTree (): GrammarTreeNode|null {
    const ruleStack = [ ...this.ruleStack ]
    const rule: GrammarRule|undefined = ruleStack.shift()
    if (!rule) return null
    let depth = 2
    const initTreeNode: GrammarTreeNode = {
      name: rule.leftSide,
      children: rule.rightSide.map(symbol => {
        return {
          name: symbol,
          children: []
        }
      })
    }

    while (ruleStack.length > 0) {
      const lastChildren: GrammarTreeNode[] = this.getChildrenInDepth(initTreeNode, 1, depth)
      const nodes = lastChildren.filter(child => this.grammar.nonTerminals.includes(child.name)).reverse()
      const rulesInLayer = ruleStack.splice(0, nodes.length)
      nodes.forEach((node, index) => {
        const ruleInLayer = rulesInLayer[index]
        ruleInLayer.rightSide.forEach(symbol => {
          node.children.push({ name: symbol, children: []})
        })
      })
      depth++
    }

    return initTreeNode
  }

  private getChildrenInDepth (tree: GrammarTreeNode, depth: number, maxDepth: number): GrammarTreeNode[] {
    if (depth === maxDepth - 1) {
      return tree.children
    }

    let newChildren: GrammarTreeNode[] = []

    tree.children.forEach(child => {
      const childChildren: GrammarTreeNode[] = this.getChildrenInDepth(child, depth + 1, maxDepth)
      newChildren = [ ...newChildren, ...childChildren]
    })

    return newChildren
  }


  private runReduceOperation (tableItem: TableStateItem, stateStack: number[]) {
    if (!tableItem.reduceRule) return
    const rightSide = tableItem.reduceRule.rightSide
    const reduceCount = rightSide?.length === 1 && rightSide.includes(Constants.EPSILON) ? 0 : rightSide?.length
    stateStack.splice(0, reduceCount)
    this.ruleStack.unshift(tableItem.reduceRule)
    const tableState: TableStateItem[] = this.tableStates[stateStack[0]]
    const reduceItem = tableState.find(item => JSON.stringify(item.symbols) === JSON.stringify([tableItem.reduceRule?.leftSide]))
    if (!reduceItem || reduceItem.operation !== 'move' || !reduceItem.gotoState){
      return { result: false, error: 'text_not_in_grammar'}
    }
    stateStack.unshift(reduceItem.gotoState)
  }

  private isRuleCorrect (rule?: GrammarRule) {
    return rule && rule.leftSide && rule.rightSide && rule.rightSide.length > 0
  }

  private loadNextTextSymbols () {
    this.nextTextSymbols = this.text.length === 0
      ? [ Constants.EPSILON ]
      : this.allSymbols.find(symbols => JSON.stringify(symbols) === JSON.stringify(this.text.slice(0, symbols.length)))
    if (this.nextTextSymbols) {
      this.text.shift()
    }
  }
}
