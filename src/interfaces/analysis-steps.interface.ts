import { TableStateItem } from "./automate-table.interface";

export interface AnalysisStep {
  stateStack: number[]
  nextSymbols: string[]
  action: TableStateItem
}
