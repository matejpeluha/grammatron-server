import { AutomateState } from "./automate-state.interface"
import Grammar from "./grammar.interface"
import { AutomateTable } from "./automate-table.interface";
import { FirstCollection, FollowCollection } from "./collections.interface";

export default interface AutomateResponse {
  isGrammarContextFree: boolean
  grammar: Grammar
  first?: FirstCollection
  follow?: FollowCollection
  states?: AutomateState[]
  numberOfStates?: number
  table?: AutomateTable
}
