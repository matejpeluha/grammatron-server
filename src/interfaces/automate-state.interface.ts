import GrammarRule from "./grammar-rule.interface";

export interface AutomateState {
  items: AutomateStateItem[]
  isProcessed?: boolean
  goto: Record<string, number>
}

export interface AutomateStateItem {
  rule: GrammarRule
  delimiterIndex: number
  expectedInput: string[][]
  isProcessed?: boolean
}

