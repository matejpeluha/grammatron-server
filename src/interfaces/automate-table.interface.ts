import GrammarRule from "./grammar-rule.interface";
import { AutomateState } from "./automate-state.interface";

export interface Automate {
  table: AutomateTable
  states: AutomateState[]
  numberOfStates: number
}

export interface AutomateTable {
  isCollisionPresent: boolean
  allTableSymbols: string[][]
  tableStates: AutomateTableStates
}

// states are rows, symbols are cells
export type AutomateTableStates = TableStateItem[][]

export interface TableStateItem {
  symbols: string[]
  operation: 'reduce'|'shift'|'move'|'accept'
  gotoState?: number
  reduceRule?: GrammarRule
}
