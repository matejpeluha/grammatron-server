
export type FirstCollection = Record<string, string[][]>
export type FollowCollection = Record<string, string[][]>
export type SetsCollection = { first: FirstCollection, follow?: FollowCollection }
export type MergeSetsResponse = { result: string[][], isUpdated: boolean }
