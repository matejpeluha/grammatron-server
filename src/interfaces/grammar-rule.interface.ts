export default interface GrammarRule {
  leftSide: string
  rightSide: string[]
}
