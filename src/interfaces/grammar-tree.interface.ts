import GrammarRule from "./grammar-rule.interface";

export interface GrammarTreeNode {
  name: string
  children: GrammarTreeNode[]
}
