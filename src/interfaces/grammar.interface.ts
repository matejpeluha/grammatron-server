import GrammarRule from "./grammar-rule.interface";

export default interface Grammar {
  k: number
  terminals: string[]
  nonTerminals: string[]
  rules: GrammarRule[]
  startSymbol: string
}
