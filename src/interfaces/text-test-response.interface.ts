import GrammarRule from "./grammar-rule.interface";
import { GrammarTreeNode } from "./grammar-tree.interface";
import { AnalysisStep } from "./analysis-steps.interface";

export interface TextTestResponse {
  result: boolean
  error?: string
  ruleStack?: GrammarRule[]
  tree?: GrammarTreeNode|null
  analysisSteps?: AnalysisStep[]
}
