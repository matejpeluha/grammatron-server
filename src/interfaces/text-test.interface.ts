import { AutomateTableStates } from "./automate-table.interface";
import Grammar from "./grammar.interface";

export interface TextTestBody {
  text: string[]
  tableStates: AutomateTableStates
  grammar: Grammar
}
