import { Module } from '@nestjs/common';
import { AutomateCreatorService } from './automate-creator.service';
import { SetFirstModule } from "../set-first/set-first.module";
import { GrammarSetsModule } from "../grammar-sets/grammar-sets.module";
import { AutomateTableCreatorModule } from "../automate-table-creator/automate-table-creator.module";

@Module({
  imports: [SetFirstModule, GrammarSetsModule, AutomateTableCreatorModule],
  providers: [AutomateCreatorService],
  exports: [AutomateCreatorService]
})
export class AutomateCreatorModule {}
