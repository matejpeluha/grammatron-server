import { Inject, Injectable } from "@nestjs/common";
import { AutomateState, AutomateStateItem } from "../../interfaces/automate-state.interface";
import Grammar from "../../interfaces/grammar.interface";
import GrammarRule from "../../interfaces/grammar-rule.interface";
import Constants from "../../constants";
import { FirstCollection } from "../../interfaces/collections.interface";
import { SetFirstService } from "../set-first/set-first.service";
import { GrammarSetsService } from "../grammar-sets/grammar-sets.service";
import { AutomateTableCreatorService } from "../automate-table-creator/automate-table-creator.service";
import { Automate } from "../../interfaces/automate-table.interface";

@Injectable()
export class AutomateCreatorService {
  @Inject(SetFirstService)
  private readonly firstService: SetFirstService
  @Inject(GrammarSetsService)
  private readonly setsService: GrammarSetsService
  @Inject(AutomateTableCreatorService)
  private readonly automateTableService: AutomateTableCreatorService
  private grammar: Grammar
  private first: FirstCollection
  private automateStates: AutomateState[]

  public createAutomate (grammar: Grammar, first: FirstCollection): Automate {
    this.grammar = grammar
    this.first = first

    this.updateGrammarByStartSymbol()

    const initState = this.createInitState()
    this.automateStates = [initState]

    let chosenState: AutomateState | undefined
    while (chosenState = this.automateStates.find(state => !state.isProcessed)) {
      chosenState.isProcessed = true
      this.createNewStatesFromState(chosenState)
    }

    const table = this.automateTableService.createAutomateTable(grammar, first, this.automateStates)


    return {
      table: table,
      numberOfStates: this.automateStates.length,
      states: this.automateStates
    }
  }

  private createNewStatesFromState (state: AutomateState) {
    const gotoSymbols = [...new Set(state.items.map(item => item.rule.rightSide[item.delimiterIndex]))].filter(Boolean)
    gotoSymbols.forEach(symbol => {
      const stateItems = state.items.filter(item => item.rule.rightSide[item.delimiterIndex] === symbol)
      let allNewStateItems: AutomateStateItem[] = []
      stateItems.forEach(item => {
        if (item.delimiterIndex + 1 <= item.rule.rightSide.length) {
          const newDelimiterIndex = item.rule.rightSide.includes(Constants.EPSILON) && item.rule.rightSide.length === 1
            ? 1
            : item.delimiterIndex + 1
          const closureItem: AutomateStateItem = { ...item, delimiterIndex: newDelimiterIndex, isProcessed: false }
          const newStateItems: AutomateStateItem[] = this.extendClosure([closureItem])
          allNewStateItems = [ ...allNewStateItems, ...newStateItems]
        }
      })
      const newState: AutomateState = { items: allNewStateItems, isProcessed: false, goto: {} }

      let stateIndex: number = this.getStateIndex(newState)

      if (stateIndex === -1) {
        this.automateStates.push(newState)
        stateIndex = this.automateStates.length - 1
      }

      if (symbol && !(symbol in state.goto)) {
        state.goto[symbol] = stateIndex
      }

    })
  }

  private getStateIndex (newState: AutomateState): number {
    return this.automateStates.findIndex(state => {
      // return JSON.stringify(state.items) === JSON.stringify(newState.items)
      return state.items.length === newState.items.length
        && newState.items.every(newStateItem => {
          return state.items.some(item => {
            return this.areItemsSame(newStateItem, item)
          })
        })
    })
  }

  private areItemsSame (item1: AutomateStateItem, item2: AutomateStateItem): boolean {
    return item1.delimiterIndex === item2.delimiterIndex
      && JSON.stringify(item1.rule) === JSON.stringify(item2.rule)
      && item1.expectedInput.length === item2.expectedInput.length
      && item1.expectedInput.every(input1 => {
        return item2.expectedInput.some(input2 => {
          return JSON.stringify(input1) === JSON.stringify(input2)
        })
      })
  }

  private createInitState (): AutomateState {
    const expectedInput: string[][] = [[Constants.EPSILON]]
    const initItems: AutomateStateItem[] = this.getAutomateStateItems(this.grammar.startSymbol, expectedInput)

    const items = this.extendClosure(initItems)

    return { items, isProcessed: false, goto: {} }
  }

  private extendClosure (items: AutomateStateItem[]): AutomateStateItem[] {
    let chosenItem: AutomateStateItem | undefined
    while (chosenItem = items.find(item => !item.isProcessed)) {
      console.log(chosenItem)
      const { rule, delimiterIndex, expectedInput} = chosenItem
      chosenItem.isProcessed = true
      if (delimiterIndex < rule.rightSide.length) {
        const nextSymbol: string = rule.rightSide[delimiterIndex]
        const afterNextSymbol: string[] = rule.rightSide.slice(delimiterIndex + 1)
        const newExpectedInput = this.getExpectedInput(expectedInput, afterNextSymbol)
        const newItems = this.getAutomateStateItems(nextSymbol, newExpectedInput)
        this.mergeUniqueItems(items, newItems)
      }
    }
    return items
  }

  private mergeUniqueItems (oldItems: AutomateStateItem[], newItems: AutomateStateItem[]) {
    newItems.forEach(newItem => {
      const isItemUnique = !oldItems.some(oldItem => {
        return JSON.stringify(oldItem.rule) === JSON.stringify(newItem.rule)
          && oldItem.delimiterIndex === newItem.delimiterIndex
          && this.areExpectedInputsSame(oldItem.expectedInput, newItem.expectedInput)
      })
      if (isItemUnique) {
        oldItems.push(newItem)
      }
    })
  }

  private areExpectedInputsSame (expected1: string[][], expected2: string[][]) {
    return expected1.every(input1 => {
      return expected2.some(input2 => JSON.stringify(input1) === JSON.stringify(input2))
    })
  }

  private getExpectedInput (oldExpectedInput: string [][], afterNextSymbol: string[]): string[][] {
    const firstOfSymbols = this.firstService.getFirstPrefixes(this.first, afterNextSymbol, this.grammar.k)
    return this.setsService.getAllUniqueCombinations(firstOfSymbols, oldExpectedInput, this.grammar.k)
  }

  private getAutomateStateItems (symbol: string, expectedInput: string[][]) {
    return this.getRulesOfSymbol(symbol)
      .map(rule => this.transformRuleToItem(rule, expectedInput))
  }

  private transformRuleToItem (rule: GrammarRule, expectedInput: string[][]) {
    const delimiterIndex = rule.rightSide.includes(Constants.EPSILON) && rule.rightSide.length === 1
      ? 1
      : 0
    return {
      rule,
      delimiterIndex,
      expectedInput: expectedInput,
      isProcessed: false
    }
  }

  private getRulesOfSymbol (symbol: string): GrammarRule[] {
    return this.grammar.rules.filter(rule => rule.leftSide === symbol)
  }

  private updateGrammarByStartSymbol ()  {
    let newStartSymbol = 'S'
    while (this.grammar.nonTerminals.includes(newStartSymbol) || this.grammar.terminals.includes(newStartSymbol)) {
      newStartSymbol += "'"
    }
    const newStartRule: GrammarRule = { leftSide: newStartSymbol, rightSide: [this.grammar.startSymbol] }
    this.grammar.rules.unshift(newStartRule)
    this.grammar.startSymbol = newStartSymbol
    this.grammar.nonTerminals.unshift(newStartSymbol)
  }
}
