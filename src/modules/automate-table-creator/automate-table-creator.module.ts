import { Module } from '@nestjs/common';
import { AutomateTableCreatorService } from './automate-table-creator.service';
import { SetFirstModule } from "../set-first/set-first.module";
import { GrammarSetsModule } from "../grammar-sets/grammar-sets.module";

@Module({
  imports: [SetFirstModule, GrammarSetsModule],
  providers: [AutomateTableCreatorService],
  exports: [AutomateTableCreatorService]
})
export class AutomateTableCreatorModule {}
