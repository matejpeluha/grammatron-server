import { Inject, Injectable } from "@nestjs/common";
import Grammar from "../../interfaces/grammar.interface";
import { FirstCollection } from "../../interfaces/collections.interface";
import { AutomateState, AutomateStateItem } from "../../interfaces/automate-state.interface";
import { AutomateTable, AutomateTableStates, TableStateItem } from "../../interfaces/automate-table.interface";
import Constants from "../../constants";
import { SetFirstService } from "../set-first/set-first.service";
import { GrammarSetsService } from "../grammar-sets/grammar-sets.service";

@Injectable()
export class AutomateTableCreatorService {
  @Inject(SetFirstService)
  private readonly firstService: SetFirstService
  @Inject(GrammarSetsService)
  private readonly setsService: GrammarSetsService
  private grammar: Grammar
  private first: FirstCollection
  private states: AutomateState[]
  private tableStates: AutomateTableStates

  public createAutomateTable (grammar: Grammar, first: FirstCollection, states: AutomateState[]): AutomateTable {
    this.grammar = grammar
    this.first = first
    this.states = states
    this.tableStates = []

    this.fillTable()
    const allTableSymbols: string [][] = this.getAllTableSymbolsFormatted()
    const isCollisionPresent = this.isCollisionPresent(this.tableStates, allTableSymbols)

    return {
      isCollisionPresent,
      allTableSymbols,
      tableStates: this.tableStates,
    }
  }

  private getAllTableSymbolsFormatted (): string[][] {
    const predefinedTableSymbols: string[][] = [
      ...this.grammar.nonTerminals.map(symbol => [symbol]),
      [Constants.EPSILON]
    ]

    const allTableSymbols = this.getAllTableSymbols(this.tableStates, predefinedTableSymbols)

    return allTableSymbols
      .filter(symbols => !(symbols.length === 1 && symbols[0] === this.grammar.startSymbol))
  }

  public getAllTableSymbols (tableStates: AutomateTableStates, predefinedTableSymbols?: string[][]) {
    const allTableSymbols: string[][] = []

    tableStates.forEach((tableState: TableStateItem[]) => {
      tableState.forEach((tableStateItem: TableStateItem) => {
        const symbols: string[] = tableStateItem.symbols
        const areSymbolsInStack = allTableSymbols.some(savedSymbols => JSON.stringify(savedSymbols) === JSON.stringify(symbols))
          || predefinedTableSymbols?.some(predefinedSymbols => JSON.stringify(predefinedSymbols) === JSON.stringify(symbols))
        if (!areSymbolsInStack) {
          allTableSymbols.unshift(symbols)
        }
      })
    })

    return [
      ...allTableSymbols.sort((a, b) => b.length - a.length),
      ...(predefinedTableSymbols || [])
      ]
  }

  public isCollisionPresent (tableStates: AutomateTableStates, allTableSymbols: string[][]): boolean {
    return tableStates.some((stateItems: TableStateItem[]) => {
      return allTableSymbols.some((symbols: string[]) => {
        return stateItems.filter((item: TableStateItem) => {
          return JSON.stringify(item.symbols) === JSON.stringify(symbols)
        }).length > 1
      });
    });
  }

  private fillTable () {
    this.states.forEach((automateState: AutomateState, index: number) => {
      this.tableStates.push([])
      if (this.isAcceptanceItemInState(automateState)) {
        this.acceptItemState(index)
      }

      this.fillStateMoves(index, automateState.goto)
      this.fillShifts(index, automateState)
      this.fillReduces(index, automateState.items)
    })
  }

  private isAcceptanceItemInState (automateState: AutomateState) {
    return automateState.items.some(item => this.isAcceptanceItem(item))
  }

  private isAcceptanceItem (item: AutomateStateItem) {
    return item.rule.leftSide === this.grammar.startSymbol
      && item.delimiterIndex === 1
      && item.expectedInput.length === 1
      && item.expectedInput[0].length === 1
      && item.expectedInput[0][0] === Constants.EPSILON
  }

  private acceptItemState (stateIndex: number) {
    const stateTableItems = this.tableStates[stateIndex]
    const newItem: TableStateItem = {
      symbols: [Constants.EPSILON],
      operation: 'accept'
    }
    stateTableItems.push(newItem)
  }

  private fillStateMoves (stateIndex: number, goto: Record<string, number>) {
    const stateTableItems = this.tableStates[stateIndex]
    Object.entries(goto).forEach(([symbol, gotoState]: [string, number]) => {
      if (this.grammar.nonTerminals.includes(symbol)) {
        const newItem: TableStateItem = {
          symbols: [symbol],
          operation: 'move',
          gotoState
        }
        if (!this.isItemInState(newItem, stateTableItems)) {
          stateTableItems.push(newItem)
        }
      }
    })
  }

  private fillShifts (stateIndex: number, { items, goto }: AutomateState) {
    const stateTableItems = this.tableStates[stateIndex]
    items.forEach((item: AutomateStateItem) => {
      const { rightSide } = item.rule
      const nextSymbol = rightSide[item.delimiterIndex]
      if (item.delimiterIndex < item.rule.rightSide.length && this.grammar.terminals.includes(nextSymbol)  && !this.isAcceptanceItem(item)) {
        const allNextSymbols = rightSide.slice(item.delimiterIndex, rightSide.length)
        const firstResult = this.firstService.getFirstPrefixes(this.first, allNextSymbols, this.grammar.k)
        const tableSymbols = this.setsService.getAllUniqueCombinations(firstResult, item.expectedInput, this.grammar.k)
        tableSymbols.forEach((symbols: string[]) => {
          const newItem: TableStateItem = {
            symbols,
            operation: 'shift',
            gotoState: goto[nextSymbol]
          }
          if (!this.isItemInState(newItem, stateTableItems)) {
            stateTableItems.push(newItem)
          }
        })
      }
    })
  }

  // here its not possible to have items from acceptance state
  private fillReduces (stateIndex: number, items: AutomateStateItem[]) {
    const stateTableItems = this.tableStates[stateIndex]
    items.forEach((item: AutomateStateItem) => {
      if (item.delimiterIndex === item.rule.rightSide.length && !this.isAcceptanceItem(item)) {
        item.expectedInput.forEach((symbols: string[]) => {
          const newItem: TableStateItem = {
            symbols,
            operation: 'reduce',
            reduceRule: item.rule
          }
          if (!this.isItemInState(newItem, stateTableItems)) {
            stateTableItems.push(newItem)
          }
        })
      }
    })
  }

  private isItemInState (newItem: TableStateItem, stateItems: TableStateItem[]) {
    return stateItems.some(item => {
      return JSON.stringify(item.symbols) === JSON.stringify(newItem.symbols)
        && JSON.stringify(item.gotoState) === JSON.stringify(newItem.gotoState)
        && JSON.stringify(item.operation) === JSON.stringify(newItem.operation)
        && JSON.stringify(item.reduceRule) === JSON.stringify(newItem.reduceRule)
    })
  }
}
