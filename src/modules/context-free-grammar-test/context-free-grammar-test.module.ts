import { Module } from '@nestjs/common';
import { ContextFreeGrammarTestService } from './context-free-grammar-test.service';

@Module({
  imports: [],
  providers: [ContextFreeGrammarTestService],
  controllers: [],
  exports: [ContextFreeGrammarTestService],
})
export class ContextFreeGrammarTestModule {}
