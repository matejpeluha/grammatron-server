import { Injectable } from '@nestjs/common';
import Grammar from "../../interfaces/grammar.interface";
import Constants from "../../constants";

@Injectable()
export class ContextFreeGrammarTestService {
  public isGrammarContextFree(grammar: Grammar): boolean {
    return (
      this.isInfoFilled(grammar)
      && !grammar.nonTerminals.includes(Constants.EPSILON)
      && !grammar.terminals.includes(Constants.EPSILON)
      && grammar.startSymbol !== Constants.EPSILON
      && grammar.nonTerminals.includes(grammar.startSymbol)
      && this.areTerminalsAndNonTerminalsDifferent(grammar)
      && this.areRulesValid(grammar)
    )
  }

  private isInfoFilled({ rules, nonTerminals, terminals, startSymbol }: Grammar): boolean {
    return !(
      !rules
      || !nonTerminals
      || !terminals
      || !startSymbol
      || rules.length < 1
      || nonTerminals.length < 1
      || terminals.length < 1
      || !Array.isArray(rules)
      || !Array.isArray(nonTerminals)
      || !Array.isArray(terminals)
      || Array.isArray(startSymbol)
    )
  }

  private areRulesValid({ rules, nonTerminals, terminals, startSymbol }: Grammar): boolean {
    let isStartSymbolUsed = false

    for (const rule of rules) {
      const { leftSide, rightSide } = rule
      if (Array.isArray(leftSide) || !Array.isArray(rightSide)) return false
      if (!nonTerminals.includes(leftSide)) return false
      if (!this.isRightSideContextFree(rightSide, [...terminals, ...nonTerminals])) return false
      if (rightSide.includes(Constants.EPSILON) && rightSide.length > 1) return false

      if (!isStartSymbolUsed) {
        isStartSymbolUsed = rule.leftSide === startSymbol
      }
    }

    return isStartSymbolUsed
  }

  private areTerminalsAndNonTerminalsDifferent({ terminals, nonTerminals }: Grammar) {
    for (const terminal of terminals) {
      if (nonTerminals.includes(terminal)) return false
    }
    return true
  }

  private isRightSideContextFree(rightSide: string[], symbols: string[]) {
    for (const rightSideSymbol of rightSide) {
      if (!symbols.includes(rightSideSymbol) && rightSideSymbol !== Constants.EPSILON) {
        return false
      }
      // let rightSideRuleLength = 0
      // for (const symbol of symbols) {
      //   const regex = new RegExp(`${symbol}`, 'gi');
      //   const regexMatch = rightSideRule.match(regex)
      //   const substringCount = regexMatch ? regexMatch.length : 0
      //   rightSideRuleLength += substringCount * symbol.length
      // }
      // if (rightSideRuleLength !== rightSideRule.length) return false
    }
    return true
  }
}
