import { Module } from '@nestjs/common';
import { CreateSymbolGroupsService } from './create-symbol-groups.service';

@Module({
  providers: [CreateSymbolGroupsService],
  exports: [CreateSymbolGroupsService]
})
export class CreateSymbolGroupsModule {}
