import { Injectable } from '@nestjs/common';
import Grammar from "../../interfaces/grammar.interface";
import GrammarRule from "../../interfaces/grammar-rule.interface";
import Constants from "../../constants";

@Injectable()
export class CreateSymbolGroupsService {
  public createUsefulNonTerminals({terminals, rules}: Grammar): string[] {
    let result: string[] = []
    let newUsefulNonTerminals: string[]

    do {
      newUsefulNonTerminals= []
      rules.forEach(rule => {
        if (!result.includes(rule.leftSide) && !newUsefulNonTerminals.includes(rule.leftSide)) {
          if (rule.rightSide.every(symbol => result.includes(symbol) || terminals.includes(symbol) || symbol === Constants.EPSILON)) {
            newUsefulNonTerminals.push(rule.leftSide)
          }
        }
      })
      result = [...result, ...newUsefulNonTerminals]
    } while(newUsefulNonTerminals.length > 0)

    return result
  }

  public createAvailableSymbols({ startSymbol, rules }: Grammar): string[] {
    let result: string[] = [startSymbol]
    let newAvailableSymbols: string[]

    do {
      newAvailableSymbols = []
      rules.forEach(({ leftSide, rightSide }: GrammarRule) => {
        if (result.includes(leftSide)) {
          rightSide.forEach(symbol => {
            if (!result.includes(symbol) && !newAvailableSymbols.includes(symbol)) {
              newAvailableSymbols.push(symbol)
            }
          })
        }
      })
      result = [...result, ...newAvailableSymbols]
    } while (newAvailableSymbols.length > 0)

    return result
  }

  public createEpsilonNonTerminals({ rules }: Grammar) {
    let result: string[] = []
    let newSymbols: string[]

    do {
      newSymbols = []
      rules.forEach(({ leftSide, rightSide }: GrammarRule) => {
        if (!result.includes(leftSide) && !newSymbols.includes(leftSide)) {
          if (rightSide.length === 1 && (rightSide[0] === Constants.EPSILON || result.includes(rightSide[0]))) {
            newSymbols.push(leftSide)
          }
        }
      })
      result = [...result, ...newSymbols]
    } while (newSymbols.length > 0)

    return result
  }
}
