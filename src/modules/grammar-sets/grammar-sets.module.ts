import { Module } from '@nestjs/common';
import { GrammarSetsService } from './grammar-sets.service';

@Module({
  providers: [GrammarSetsService],
  exports: [GrammarSetsService]
})
export class GrammarSetsModule {}
