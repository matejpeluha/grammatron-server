import { Injectable } from "@nestjs/common";
import { MergeSetsResponse } from "../../interfaces/collections.interface";
import Constants from "../../constants";

@Injectable()
export class GrammarSetsService {
  public mergeNewSetToOld (newSet: string[][], oldSet: string[][]): MergeSetsResponse {
    const uniqueNewPrefixes = this.filterPrefixes(newSet, oldSet)
    return  uniqueNewPrefixes.length > 0
      ? { result: [...oldSet, ...uniqueNewPrefixes], isUpdated: true }
      : { result: [...oldSet], isUpdated: false }
  }

  public filterPrefixes (prefixes: string[][], oldPrefixes: string[][]): string[][] {
    return prefixes.filter((prefix: string[]) => {
      return oldPrefixes.every((oldPrefix: string[]) => {
        return JSON.stringify(oldPrefix) !== JSON.stringify(prefix)
      })
    })
  }

  public getAllUniqueCombinations (symbolSet1: string[][], symbolSet2: string[][], k: number) {
    const combinations: string[][] = this.getAllCombinations(symbolSet1, symbolSet2)
    return this.getOnlyNewPrefixes(combinations, k)
  }

  public getOnlyNewPrefixes (combinations: string[][], k: number) {
    const newPrefixes: string[][] = []
    combinations.forEach(combination => {
      const newPrefix = k < combination.length ? combination.slice(0, k) : [...combination]
      const isNewPrefix = newPrefixes.every(prefix => JSON.stringify(prefix) !== JSON.stringify(newPrefix))
      if (isNewPrefix) {
        newPrefixes.push(newPrefix)
      }
    })
    return newPrefixes
  }

  public getAllCombinations (symbolSet1: string[][], symbolSet2: string[][]): string[][] {
    const symbolPrefixes: string[][][] = [
      symbolSet1.map(prefix => !prefix.includes(Constants.EPSILON) ? prefix : []),
      symbolSet2.map(prefix => !prefix.includes(Constants.EPSILON) ? prefix : [])
    ]

    const combinations: string[][] = []
    this.getCombinations(symbolPrefixes, combinations)

    return combinations.map(combination => combination.length === 0 ? [Constants.EPSILON] : combination)
  }

  public getCombinations (input: string[][][], output: string[][], position= 0, path: string[][] = []) {
    if (position < input.length) {
      const item = input[position]
      for (let i = 0; i < item.length; ++i) {
        const value = item[i]
        path.push(value)
        this.getCombinations(input, output, position + 1, path)
        path.pop()
      }
    } else {
      let mergedPath: string[] = []
      path.forEach(pathPart => mergedPath = [...mergedPath, ...pathPart])
      output.push(mergedPath.slice())
    }
  }
}
