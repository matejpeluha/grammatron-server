import { Module } from '@nestjs/common';
import { ReduceGrammarService } from './reduce-grammar.service';
import { CreateSymbolGroupsModule } from "../create-symbol-groups/create-symbol-groups.module";

@Module({
  imports: [CreateSymbolGroupsModule],
  providers: [ReduceGrammarService],
  controllers: [],
  exports: [ReduceGrammarService]
})
export class ReduceGrammarModule {}
