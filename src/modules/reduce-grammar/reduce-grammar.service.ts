import { Inject, Injectable } from "@nestjs/common";
import Grammar from "../../interfaces/grammar.interface";
import { CreateSymbolGroupsService } from "../create-symbol-groups/create-symbol-groups.service";
import Constants from "../../constants";
import { MergeSetsResponse } from "../../interfaces/collections.interface";

@Injectable()
export class ReduceGrammarService {
  @Inject(CreateSymbolGroupsService)
  private readonly symbolService: CreateSymbolGroupsService

  public reduceGrammar(grammar: Grammar) {
    const usefulNonTerminals = this.symbolService.createUsefulNonTerminals(grammar)
    if (!usefulNonTerminals.includes(grammar.startSymbol)) {
      return {  usefulNonTerminals, reducedGrammar: [] }
    }
    this.reduceByUsefulNonTerminals(grammar, usefulNonTerminals)
    const availableSymbols = this.symbolService.createAvailableSymbols(grammar)
    this.reduceByAvailableSymbols(grammar, availableSymbols)
  }

  private reduceByAvailableSymbols(grammar: Grammar, availableSymbols: string[]) {
    grammar.nonTerminals = grammar.nonTerminals.filter(symbol => availableSymbols.includes(symbol))
    grammar.terminals = grammar.terminals.filter(symbol => availableSymbols.includes(symbol))
    grammar.rules = grammar.rules.filter(rule => {
      return availableSymbols.includes(rule.leftSide)
        && rule.rightSide.every(symbol => availableSymbols.includes(symbol))
    })
  }

  private reduceByUsefulNonTerminals(grammar: Grammar, usefulNonTerminals: string[]) {
    grammar.nonTerminals = grammar.nonTerminals.filter(symbol => usefulNonTerminals.includes(symbol))
    const allowedLeftSideRuleSymbols = [...usefulNonTerminals, ...grammar.terminals, Constants.EPSILON]
    grammar.rules = grammar.rules.filter(rule => {
     return usefulNonTerminals.includes(rule.leftSide)
       && rule.rightSide.every(symbol => allowedLeftSideRuleSymbols.includes(symbol))
    })
  }
}
