import { Module } from '@nestjs/common';
import { SetFirstService } from './set-first.service';
import { CreateSymbolGroupsModule } from "../create-symbol-groups/create-symbol-groups.module";
import { ReduceGrammarModule } from "../reduce-grammar/reduce-grammar.module";
import { GrammarSetsModule } from "../grammar-sets/grammar-sets.module";

@Module({
  imports: [CreateSymbolGroupsModule, ReduceGrammarModule, GrammarSetsModule],
  providers: [SetFirstService],
  exports: [SetFirstService]
})
export class SetFirstModule {}
