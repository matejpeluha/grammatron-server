import { Inject, Injectable } from "@nestjs/common";
import { CreateSymbolGroupsService } from "../create-symbol-groups/create-symbol-groups.service";
import Grammar from "../../interfaces/grammar.interface";
import { FirstCollection } from "../../interfaces/collections.interface";
import Constants from "../../constants";
import GrammarRule from "../../interfaces/grammar-rule.interface";
import { GrammarSetsService } from "../grammar-sets/grammar-sets.service";

@Injectable()
export class SetFirstService {
  @Inject(CreateSymbolGroupsService)
  private readonly symbolService: CreateSymbolGroupsService
  @Inject(GrammarSetsService)
  private readonly setsService: GrammarSetsService

  public createSetFirst (grammar: Grammar): FirstCollection {
    const first: FirstCollection = this.initSetFirst(grammar)
    this.fillFirst(grammar, first)
    this.removeEmptyPrefixes(first)

    // console.log("TEST:", this.getFirstPrefixes(first, ["S","A","c", "B"], grammar.k))

    return first
  }

  private fillFirst (grammar: Grammar, first: FirstCollection): void {
    let isNewPrefix = true
    while (isNewPrefix) {
      isNewPrefix = false
      grammar.rules.forEach(({ leftSide, rightSide}: GrammarRule) => {
        const newPrefixes = this.getFirstPrefixes(first, rightSide, grammar.k)
        const { result, isUpdated } = this.setsService.mergeNewSetToOld(newPrefixes, first[leftSide])
        isNewPrefix = isUpdated ? true : isNewPrefix
        first[leftSide] = result
      })
    }
  }

  public getMultipleFirstPrefixes(first: FirstCollection, multipleSymbols: string[][], k: number): string[][] {
    const prefixes: string[][] = []

    multipleSymbols.forEach(symbols => {
      const newPrefixes = this.getFirstPrefixes(first, symbols, k)
      newPrefixes.forEach(newPrefix => {
        const isUniquePrefix = prefixes.every(prefix => JSON.stringify(prefix) !== JSON.stringify(newPrefix))
        if (isUniquePrefix) {
          prefixes.push(newPrefix)
        }
      })
    })

    return prefixes
  }

  public getFirstPrefixes(first: FirstCollection, symbols: string[], k: number): string[][] {
    if (symbols.length === 1 && symbols.includes(Constants.EPSILON)) return [[Constants.EPSILON]]
    const combinations: string[][] = this.getAllFirstCombinations(first, symbols)
    return this.setsService.getOnlyNewPrefixes(combinations, k)
  }

  private getAllFirstCombinations = (first: FirstCollection, symbols: string[]): string[][] => {
    const symbolPrefixes: string[][][] = []
    symbols.forEach(symbol => {
      const oldPrefixes: string[][] = first[symbol].map(prefix => !prefix.includes(Constants.EPSILON) ? prefix : [])
      symbolPrefixes.push(oldPrefixes)
    })
    const combinations: string[][] = []
    this.setsService.getCombinations(symbolPrefixes, combinations)

    return combinations.map(combination => combination.length === 0 ? [Constants.EPSILON] : combination)
  }

  private initSetFirst (grammar: Grammar): FirstCollection {
    const first: FirstCollection = {}

    first[Constants.EPSILON] = [[Constants.EPSILON]]

    grammar.terminals.forEach(symbol => {
      first[symbol] = [[symbol]]
    })

    grammar.nonTerminals.forEach(symbol => {
      first[symbol] = []
    })

    const epsilonNonTerminals = this.symbolService.createEpsilonNonTerminals(grammar)

    epsilonNonTerminals.forEach(symbol => {
      first[symbol].push([Constants.EPSILON])
    })

    return first
  }

  private removeEmptyPrefixes (first: FirstCollection): void {
    Object.entries(first).forEach(([symbol, prefixes]: [string, string[][]]) => {
      first[symbol] = prefixes.filter(prefix => prefix.length > 0)
    })
  }
}
