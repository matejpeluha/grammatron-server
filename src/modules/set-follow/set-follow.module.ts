import { Module } from '@nestjs/common';
import { SetFollowService } from './set-follow.service';
import { SetFirstModule } from "../set-first/set-first.module";
import { ReduceGrammarModule } from "../reduce-grammar/reduce-grammar.module";
import { GrammarSetsModule } from "../grammar-sets/grammar-sets.module";

@Module({
  imports: [ReduceGrammarModule, SetFirstModule, GrammarSetsModule],
  providers: [SetFollowService],
  exports: [SetFollowService]
})
export class SetFollowModule {}
