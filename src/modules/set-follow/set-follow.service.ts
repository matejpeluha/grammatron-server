import { Inject, Injectable } from "@nestjs/common";
import Grammar from "../../interfaces/grammar.interface";
import { FirstCollection, FollowCollection } from "../../interfaces/collections.interface";
import Constants from "../../constants";
import GrammarRule from "../../interfaces/grammar-rule.interface";
import { SetFirstService } from "../set-first/set-first.service";
import { GrammarSetsService } from "../grammar-sets/grammar-sets.service";

@Injectable()
export class SetFollowService {
  @Inject(SetFirstService)
  private readonly firstService: SetFirstService
  @Inject(GrammarSetsService)
  private readonly setsService: GrammarSetsService

  public createSetFollow (grammar: Grammar, first: FollowCollection) {
    const follow: FollowCollection = this.initSetFollow(grammar)
    this.fillFollow(grammar, first, follow)
    return follow
  }

  private fillFollow (grammar: Grammar, first: FirstCollection, follow: FollowCollection): void {
    let isNewPrefix = true
    while (isNewPrefix) {
      isNewPrefix = false
      grammar.rules.forEach((rule: GrammarRule) => {
        const isUpdated = this.updateFollowByRule(grammar, rule, first, follow)
        isNewPrefix = isNewPrefix || isUpdated
      })
    }
  }

  private updateFollowByRule (grammar: Grammar, rule: GrammarRule, first: FirstCollection, follow: FollowCollection): boolean {
    let isNewPrefix = false

    rule.rightSide.forEach((symbol: string, index: number )=> {
      if (grammar.nonTerminals.includes(symbol)) {
        const nextSymbols: string[] = index === rule.rightSide.length - 1
          ? [Constants.EPSILON]
          : rule.rightSide.slice(index + 1, rule.rightSide.length)
        const firstResult: string[][] = this.firstService.getFirstPrefixes(first, nextSymbols, grammar.k)
        const followResult: string[][] = follow[rule.leftSide]
        const isUpdated = this.updateFollow(firstResult, followResult, follow, symbol, grammar.k)
        isNewPrefix = isNewPrefix || isUpdated
      }
    })

    return isNewPrefix
  }

  private updateFollow (firstResult: string[][], followResult: string[][], follow: FollowCollection, followSymbol: string, k: number): boolean {
    const newPrefixes: string[][] = this.setsService.getAllUniqueCombinations(firstResult, followResult, k)

    const merged =  this.setsService.mergeNewSetToOld(newPrefixes, follow[followSymbol])
    follow[followSymbol] = merged.result

    return merged.isUpdated
  }

  private initSetFollow (grammar: Grammar): FirstCollection {
    const follow: FollowCollection = {}

    grammar.nonTerminals.forEach(symbol => {
      follow[symbol] = []
    })

    follow[grammar.startSymbol].push([Constants.EPSILON])

    return follow
  }
}
