import { Module } from '@nestjs/common';
import { TransformGrammarUrlService } from './transform-grammar-url.service';

@Module({
  providers: [TransformGrammarUrlService],
  exports: [TransformGrammarUrlService]
})
export class TransformGrammarUrlModule {}
