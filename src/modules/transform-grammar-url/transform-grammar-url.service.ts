import { Injectable } from '@nestjs/common';
import Grammar from "../../interfaces/grammar.interface";

@Injectable()
export class TransformGrammarUrlService {
  public transformGrammar(grammar: Grammar) {
    if (!Array.isArray(grammar.terminals)) {
      grammar.terminals = [grammar.terminals]
    }
    grammar.terminals = grammar.terminals.map(symbol => symbol.replace(/\s/g, ''))
    if (!Array.isArray(grammar.nonTerminals)) {
      grammar.nonTerminals = [grammar.nonTerminals]
    }
    grammar.nonTerminals = grammar.nonTerminals.map(symbol => symbol.replace(/\s/g, ''))
    grammar.rules.forEach(rule => {
      if (!Array.isArray(rule.rightSide)) {
        rule.rightSide = [rule.rightSide]
      }
      rule.leftSide = rule.leftSide.replace(/\s/g, '')
      rule.rightSide = rule.rightSide.map(symbol => symbol.replace(/\s/g, ''))
    })
    if (!grammar.k || !Number(grammar.k) || Number(grammar.k) < 1) {
      grammar.k = 1
    } else {
      grammar.k = Number(grammar.k)
    }
  }
}
